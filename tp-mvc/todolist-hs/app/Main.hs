import Board
import View

-- import Text.Read (readMaybe)

loop :: Board -> IO ()
loop b = do
    putStrLn ""
    printBoard b
    
    a <- getLine
    case words a of
        ["quit"] -> putStr "bye bye"
        ("add":xs)-> 
            let
                (i, b1) =  addTodo (unwords xs) b
            in loop b1
        ["done",xs] -> loop $ toDone (read xs) b
        _ -> loop b
main :: IO ()
main = loop newBoard

