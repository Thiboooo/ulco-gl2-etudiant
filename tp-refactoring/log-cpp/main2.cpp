#include <fstream>
#include <functional>
#include <iostream>

using logFunc_t = std::function<void(const std::string &)>;


int add3(int n) {
    return n+3;
}

int mul2(int n) {
    return n*2;
}

int mycompute(logFunc_t logFun , int v0) {
    logFun("add3 " + std::to_string(v0));
    const int v1 = add3(v0);
    logFun("mul2 " + std::to_string(v1));
    const int v2 = add3(v1);
    return v2;
}

int main() {
    std::cout << "this is log-cpp" << std::endl;
    logFunc_t f = [](const std::string & m) { std::cout << m << std::endl; };
    mycompute(f,12);

    std::cout << "this is log-cpp" << std::endl;
    logFunc_t x = [](const std::string & m) { };
    mycompute(x,12);
    return 0;
}

