#pragma once

#include <string>
#include <vector>

class Title {
    public:
        virtual std::string getTitle() const = 0;
};

