#pragma once
#include "Board.hpp"

class NumBoard:public Board{
    private:
        int nextId;
    public:
        NumBoard():nextId(1){}
        void addNum(const std::string &t){
            std::string tmp = std::to_string(nextId) + ". " + t;
            add(tmp);
            nextId++;
        }

        virtual std::string getTitle() const override {
            return "NumBoard";
        }
};