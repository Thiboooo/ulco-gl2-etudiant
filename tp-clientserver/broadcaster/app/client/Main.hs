{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text.IO as T
import qualified Network.WebSockets as WS

main :: IO ()
main = WS.runClient "0.0.0.0" 9000 "" handleConn

handleConn :: WS.Connection -> IO ()
handleConn conn = do
    msgFromSrv <- WS.receiveDataMessage conn
    msgFromSrv2 <- WS.receiveDataMessage conn
    T.putStrLn $ WS.fromDataMessage msgFromSrv
    T.putStrLn $ WS.fromDataMessage msgFromSrv2
    handleConn conn

