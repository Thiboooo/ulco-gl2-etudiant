{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Network.WebSockets as WS

import Control.Concurrent
import Control.Exception (finally)

import Net

type Var = MVar (Net WS.Connection)

main :: IO ()
main = do
    var <- newMVar newNet
    WS.runServer "0.0.0.0" 9000 (serverApp var)

serverApp :: Var -> WS.PendingConnection -> IO ()
serverApp var pc = do
    conn <- WS.acceptRequest pc
    iConn <- modifyMVar var (return . addConn conn)
    WS.withPingThread conn 30 (return ())
        (finally (handleConn var conn) (handleQuit var iConn))

handleConn :: WS.Connection -> IO ()
handleConn conn = do
    msg <- WS.receiveDataMessage conn
    let txt = (WS.receiveDataMessage msg :: T.Text)
    modifyMVar_ var $ \net -> do
        mapInNet (\c -> WS.sendTextData c txt) net
        return net
    handleConn var conn

handleQuit :: Var -> Int -> IO ()
handleQuit var iConn = modifyMVar_ var (return . rmConn iConn)

handleInput :: Var -> IO ()
handleInput var = do
    msg <- T.getLine
    modifyMVar_ var  $ \net -> do
        mapInNet (\c -> WS.sendTextData c msg) net
        return net
    handleInput var

